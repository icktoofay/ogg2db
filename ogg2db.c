#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>

#include <sqlite3.h>
#include <ogg/ogg.h>

static const char *program_name = "ogg2db";

static const char *const schema =
	"CREATE TABLE packet ("
	"packet BLOB NOT NULL, "
	"bos INTEGER NOT NULL, "
	"eos INTEGER NOT NULL, "
	"granulepos INTEGER NOT NULL, "
	"packetno INTEGER NOT NULL, "
	"serialno INTEGER NOT NULL)";

static const char *const insert_sql =
	"INSERT INTO packet (packet, bos, eos, granulepos, packetno, serialno) "
	"VALUES (?, ?, ?, ?, ?, ?)";

struct stream_table {
	struct stream_table *next;
	ogg_stream_state stream;
};

static ogg_stream_state *find_stream(struct stream_table **table, int needle) {
	int rc;
	struct stream_table *current;
	for(current = *table; current; current = current->next) {
		if(current->stream.serialno == needle) {
			return &current->stream;
		}
	}
	current = malloc(sizeof(struct stream_table));
	if(!current) {
		return NULL;
	}
	rc = ogg_stream_init(&current->stream, needle);
	if(rc == -1) {
		free(current);
		return NULL;
	}
	current->next = *table;
	*table = current;
	return &current->stream;
}

static void free_stream_table(struct stream_table *streams) {
	struct stream_table *next;
	while(streams) {
		ogg_stream_clear(&streams->stream);
		next = streams->next;
		free(streams);
		streams = next;
	}
}

int main(int argc, char **argv) {
	int rc;
	FILE *fp = NULL;
	sqlite3 *db = NULL;
	sqlite3_stmt *stmt = NULL;
	int sync_valid = 0;
	ogg_sync_state sync;
	char *buffer;
	struct stream_table *streams = NULL;
	ogg_stream_state *cur_stream;
	ogg_page page;
	ogg_packet packet;

	if(argc > 0) {
		const char *last_slash = strrchr(argv[0], '/');
		if(last_slash) {
			program_name = last_slash + 1;
		}else{
			program_name = argv[0];
		}
	}
	if(argc != 3) {
		fprintf(stderr, "usage: %s file.ogg file.db\n", program_name);
		rc = EXIT_FAILURE;
		goto cleanup;
	}

	fp = fopen(argv[1], "rb");
	if(!fp) {
		fprintf(stderr, "%s: could not open: %s\n",
			argv[1], strerror(errno));
		rc = EXIT_FAILURE;
		goto cleanup;
	}

	rc = sqlite3_open(argv[2], &db);
	if(rc != SQLITE_OK) {
		fprintf(stderr, "%s: could not open: %s\n",
			argv[2], sqlite3_errmsg(db));
		rc = EXIT_FAILURE;
		goto cleanup;
	}

	rc = sqlite3_exec(db, "BEGIN", NULL, NULL, NULL);
	if(rc != SQLITE_OK) {
		fprintf(stderr, "failed to begin transaction\n");
		rc = EXIT_FAILURE;
		goto cleanup;
	}

	rc = sqlite3_exec(db, schema, NULL, NULL, NULL);
	if(rc != SQLITE_OK) {
		fprintf(stderr, "failed to create schema\n");
		rc = EXIT_FAILURE;
		goto cleanup;
	}

	rc = sqlite3_prepare_v2(db, insert_sql, -1, &stmt, NULL);
	if(rc != SQLITE_OK) {
		fprintf(stderr, "failed to prepare INSERT\n");
		rc = EXIT_FAILURE;
		goto cleanup;
	}

	rc = ogg_sync_init(&sync);
	if(rc == -1) {
		fprintf(stderr, "failed to initialize sync state\n");
		rc = EXIT_FAILURE;
		goto cleanup;
	}
	sync_valid = 1;

	for(;;) {
		rc = ogg_sync_pageout(&sync, &page);
		if(rc != 1) {
			buffer = ogg_sync_buffer(&sync, 0x1000);
			rc = fread(buffer, 1, 0x1000, fp);
			if(rc == -1) {
				fprintf(stderr, "%s: failed to read: %s\n",
					argv[1], strerror(errno));
				rc = EXIT_FAILURE;
				goto cleanup;
			}
			if(rc == 0) {
				break;
			}
			rc = ogg_sync_wrote(&sync, rc);
			if(rc == -1) {
				fprintf(stderr, "sync error\n");
				rc = EXIT_FAILURE;
				goto cleanup;
			}
			continue;
		}
		cur_stream = find_stream(&streams, ogg_page_serialno(&page));
		if(!cur_stream) {
			fprintf(stderr, "failed to create stream\n");
			rc = EXIT_FAILURE;
			goto cleanup;
		}
		rc = ogg_stream_pagein(cur_stream, &page);
		if(rc == -1) {
			fprintf(stderr, "failed to consume page\n");
			rc = EXIT_FAILURE;
			goto cleanup;
		}
		for(;;) {
			rc = ogg_stream_packetout(cur_stream, &packet);
			if(rc == -1) {
				continue;
			}
			if(rc == 0) {
				break;
			}
			rc = sqlite3_bind_blob64(
				stmt, 1, packet.packet, packet.bytes,
				SQLITE_TRANSIENT);
			if(rc != SQLITE_OK) {
				fprintf(stderr, "failed to bind packet\n");
				rc = EXIT_FAILURE;
				goto cleanup;
			}
			rc = sqlite3_bind_int(stmt, 2, packet.b_o_s);
			if(rc != SQLITE_OK) {
				fprintf(stderr, "failed to bind B.O.S\n");
				rc = EXIT_FAILURE;
				goto cleanup;
			}
			rc = sqlite3_bind_int(stmt, 3, packet.e_o_s);
			if(rc != SQLITE_OK) {
				fprintf(stderr, "failed to bind E.O.S\n");
				rc = EXIT_FAILURE;
				goto cleanup;
			}
			rc = sqlite3_bind_int64(stmt, 4, packet.granulepos);
			if(rc != SQLITE_OK) {
				fprintf(stderr, "failed to bind granulepos\n");
				rc = EXIT_FAILURE;
				goto cleanup;
			}
			rc = sqlite3_bind_int64(stmt, 5, packet.packetno);
			if(rc != SQLITE_OK) {
				fprintf(stderr, "failed to bind packetno\n");
				rc = EXIT_FAILURE;
				goto cleanup;
			}
			rc = sqlite3_bind_int64(stmt, 6, cur_stream->serialno);
			if(rc != SQLITE_OK) {
				fprintf(stderr, "failed to bind serialno\n");
				rc = EXIT_FAILURE;
				goto cleanup;
			}
			sqlite3_step(stmt);
			rc = sqlite3_reset(stmt);
			if(rc != SQLITE_OK) {
				fprintf(stderr, "failed to insert\n");
				rc = EXIT_FAILURE;
				goto cleanup;
			}
			rc = sqlite3_clear_bindings(stmt);
			if(rc != SQLITE_OK) {
				fprintf(stderr, "failed to clear bindings\n");
				rc = EXIT_FAILURE;
				goto cleanup;
			}
		}
	}

	rc = sqlite3_exec(db, "COMMIT", NULL, NULL, NULL);
	if(rc != SQLITE_OK) {
		fprintf(stderr, "failed to commit transaction\n");
		rc = EXIT_FAILURE;
		goto cleanup;
	}

	rc = EXIT_SUCCESS;
cleanup:
	if(fp) {
		fclose(fp);
	}
	if(stmt) {
		sqlite3_finalize(stmt);
	}
	if(db) {
		sqlite3_close(db);
	}
	if(sync_valid) {
		ogg_sync_clear(&sync);
	}
	if(streams) {
		free_stream_table(streams);
	}
	return rc;
}
