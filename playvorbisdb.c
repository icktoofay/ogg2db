#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>

#include <sqlite3.h>
#include <ogg/ogg.h>
#include <vorbis/codec.h>
#include <ao/ao.h>

static const char *program_name = "playvorbisdb";

static const char *const query =
	"SELECT packet, bos, eos, granulepos, packetno "
	"FROM packet ORDER BY rowid";

static int fetch_packet(sqlite3_stmt *stmt, ogg_packet *packet) {
	int rc = sqlite3_step(stmt);
	if(rc == SQLITE_ROW) {
		packet->packet = (void *)sqlite3_column_blob(stmt, 0);
		if(!packet->packet) {
			return SQLITE_NOMEM;
		}
		packet->bytes = sqlite3_column_bytes(stmt, 0);
		packet->b_o_s = sqlite3_column_int(stmt, 1);
		packet->e_o_s = sqlite3_column_int(stmt, 2);
		packet->granulepos = sqlite3_column_int64(stmt, 3);
		packet->packetno = sqlite3_column_int64(stmt, 4);
	}
	return rc;
}

static int play_pcm(
		ao_device *device,
		float **float_pcm,
		int channels,
		int samples) {
	int i, j;
	int rc;
	short *short_pcm;
	float sample;
	size_t num_bytes;
	if(channels <= 0 || samples <= 0) {
		return 0;
	}
	if((size_t)samples >= ((size_t)-1) / sizeof(short) / (size_t)channels) {
		return -1;
	}
	num_bytes = samples * channels * sizeof(short);
	if(num_bytes > (size_t)(uint_32)-1) {
		return -1;
	}
	short_pcm = malloc(num_bytes);
	if(!short_pcm) {
		return -1;
	}
	for(i = 0; i < samples; ++i) {
		for(j = 0; j < channels; ++j) {
			sample = float_pcm[j][i];
			if(sample < -1) {
				sample = -1;
			}
			if(sample > 1) {
				sample = 1;
			}
			short_pcm[i * channels + j] = (short)(sample * 0x7FFF);
		}
	}
	rc = ao_play(device, (char *)short_pcm, (uint_32)num_bytes);
	free(short_pcm);
	if(!rc) {
		return -1;
	}
	return 0;
}

int main(int argc, char **argv) {
	int i;
	int rc;
	sqlite3 *db = NULL;
	sqlite3_stmt *stmt = NULL;
	ogg_packet packet;
	vorbis_info info;
	vorbis_comment comment;
	int dsp_valid = 0;
	vorbis_dsp_state dsp;
	int block_valid = 0;
	vorbis_block block;
	float **pcm;
	int ao_inited = 0;
	ao_sample_format format;
	ao_device *device = NULL;

	vorbis_info_init(&info);
	vorbis_comment_init(&comment);

	if(argc > 0) {
		const char *last_slash = strrchr(argv[0], '/');
		if(last_slash) {
			program_name = last_slash + 1;
		}else{
			program_name = argv[0];
		}
	}

	if(argc != 2) {
		fprintf(stderr, "usage: %s file.db\n", program_name);
		rc = EXIT_FAILURE;
		goto cleanup;
	}

	rc = sqlite3_open(argv[1], &db);
	if(rc != SQLITE_OK) {
		fprintf(stderr, "%s: failed to open: %s\n",
			argv[1], sqlite3_errmsg(db));
		rc = EXIT_FAILURE;
		goto cleanup;
	}

	rc = sqlite3_prepare_v2(db, query, -1, &stmt, NULL);
	if(rc != SQLITE_OK) {
		fprintf(stderr, "failed to prepare query: %s\n",
			sqlite3_errmsg(db));
		rc = EXIT_FAILURE;
		goto cleanup;
	}

	for(i = 0; i < 3; ++i) {
		rc = fetch_packet(stmt, &packet);
		if(rc == SQLITE_DONE) {
			fprintf(stderr, "premature EOS\n");
			rc = EXIT_FAILURE;
			goto cleanup;
		}
		if(rc != SQLITE_ROW) {
			fprintf(stderr, "error fetching header packet: %s\n",
				sqlite3_errmsg(db));
			rc = EXIT_FAILURE;
			goto cleanup;
		}
		rc = vorbis_synthesis_headerin(&info, &comment, &packet);
		if(rc) {
			fprintf(stderr, "error interpreting header\n");
			rc = EXIT_FAILURE;
			goto cleanup;
		}
	}

	ao_initialize();
	ao_inited = 1;

	format.bits = 16;
	format.rate = info.rate;
	format.channels = info.channels;
	format.byte_format = AO_FMT_NATIVE;
	format.matrix = NULL;

	rc = ao_default_driver_id();
	if(rc == -1) {
		fprintf(stderr, "no default ao driver found\n");
		rc = EXIT_FAILURE;
		goto cleanup;
	}
	device = ao_open_live(rc, &format, NULL);
	if(!device) {
		fprintf(stderr, "failed to open device\n");
		rc = EXIT_FAILURE;
		goto cleanup;
	}

	rc = vorbis_synthesis_init(&dsp, &info);
	if(rc) {
		fprintf(stderr, "error initializing DSP state\n");
		rc = EXIT_FAILURE;
		goto cleanup;
	}
	dsp_valid = 1;

	for(;;) {
		rc = fetch_packet(stmt, &packet);
		if(rc == SQLITE_DONE) {
			break;
		}
		if(rc != SQLITE_ROW) {
			fprintf(stderr, "error fetching packet\n");
			rc = EXIT_FAILURE;
			goto cleanup;
		}

		rc = vorbis_block_init(&dsp, &block);
		if(rc) {
			fprintf(stderr, "failed to initialize block\n");
			rc = EXIT_FAILURE;
			goto cleanup;
		}
		block_valid = 1;

		rc = vorbis_synthesis(&block, &packet);
		if(rc) {
			fprintf(stderr, "error decoding packet\n");
			rc = EXIT_FAILURE;
			goto cleanup;
		}

		rc = vorbis_synthesis_blockin(&dsp, &block);
		if(rc) {
			fprintf(stderr,
				"error submitting block to reassembly layer\n");
			rc = EXIT_FAILURE;
			goto cleanup;
		}

		i = vorbis_synthesis_pcmout(&dsp, &pcm);
		rc = play_pcm(device, pcm, info.channels, i);
		if(rc) {
			fprintf(stderr, "error playing PCM\n");
			rc = EXIT_FAILURE;
			goto cleanup;
		}
		rc = vorbis_synthesis_read(&dsp, i);
		if(rc) {
			fprintf(stderr, "failed to mark PCM as read\n");
			rc = EXIT_FAILURE;
			goto cleanup;
		}

		rc = vorbis_block_clear(&block);
		if(rc) {
			fprintf(stderr, "failed to clear block\n");
			rc = EXIT_FAILURE;
			goto cleanup;
		}
		block_valid = 0;
	}

	rc = EXIT_SUCCESS;
cleanup:
	if(device) {
		ao_close(device);
	}
	if(ao_inited) {
		ao_shutdown();
	}
	if(block_valid) {
		vorbis_block_clear(&block);
	}
	if(dsp_valid) {
		vorbis_dsp_clear(&dsp);
	}
	if(stmt) {
		sqlite3_finalize(stmt);
	}
	if(db) {
		sqlite3_close(db);
	}
	vorbis_info_clear(&info);
	vorbis_comment_clear(&comment);
	return rc;
}
